//import dependencies
const Product = require("../models/product");
const auth = require("../auth"); //import auth.js file to use its authorization functions

//create a new product
module.exports.createProduct = (body) => {
		let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false; //product was not saved
		}else{
			return true; //product was successfully saved
		}
	})	
}

//get all products
//module.exports.getProducts = () => {
//	return Product.find({}).then(result => {
//		return result; //find all products, then return the //result
//	})
//}

//get all ACTIVE products
module.exports.getProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result; //find all products, then return the result
	})
}


//get a specific product
module.exports.getProduct = (params) => {
	//findById is a Mongoose operation that just finds a document by its ID
	return Product.findById(params.productId).then(result => {
		return result;
})

}

//update specific product
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name, 
		description: body.description,
		price: body.price 
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, err) => {

		//error handling
		if(err){
			return false; 
		}else{
			return true; 
		}
	})
}

//archive specific Product
module.exports.archiveProduct = (params) => {
	let archivedProduct = {  
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, err) => {

		//error handling
		if(err){
			return false; 
		}else{
			return true; 
		}
	})
}

//Non-admin User Checkout 
module.exports.checkout = (params, body) => {
	let checkout = {
		productName: body.productName, 
		productDescription: body.productDescription,
		productPrice: body.productPrice 
	}

	return checkout.find({}).then(result => {
		return result;
	})
}
