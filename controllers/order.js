const Order = require("../models/order");
const bcrypt = require("bcrypt") //is used to encrypt our passwords 
//encryption is a way to hide your data 
//e.g. when logging into Facecebook, you give your password as "password123"
//The value in the database is not stored as "password123" but instead a random set of letters and numbers (e.g. zthvdnq78@ga#jmYT!=00)

const auth = require("../auth") //import auth.js file to use its authorization functions

//create a new order
module.exports.placeOrder = (data) => {
		let newOrder = new Order({
		userId: data.userId,
		products: data.products,
		totalAmount: data.totalAmount
	})

	return newOrder.save().then((orders, error) => {
		if(error){
			return false; 
		}else{
			return true; 
		}
	})	
}

//retrieve all orders
module.exports.getOrders = () => {
	return Order.find({}).then(result => {
		return result; 
	})
}
