const express = require("express");
const router = express.Router(); 
const orderController = require("../controllers/order");
const auth = require("../auth");

//route for placing an order 
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		products: req.body.products,
		totalAmount: req.body.totalAmount
		}

	if(auth.decode(req.headers.authorization).isAdmin){
		res.send({auth: "failed"})
	}else{
		orderController.placeOrder(data).then(resultFromController => res.send(resultFromController))
	}
})

//route for retrieving all orders
router.get("/", (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		orderController.getOrders().then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}
})

module.exports = router; 
