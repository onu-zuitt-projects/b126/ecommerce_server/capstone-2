const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	userId: String,
	products: [
		{
			productId: String, 
			price: Number,
			quantity: Number 
		}
	],
	totalAmount: Number,
	purchasedOn: {
		type: Date,
		default: new Date() 
	}			 
})

//use module.exports to export the model as a module, so that it can be imported in other files 
module.exports = mongoose.model("Order", orderSchema)

